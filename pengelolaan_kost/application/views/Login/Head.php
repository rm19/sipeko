<!DOCTYPE html>
<html lang="en">
<head>
  <title>K7-KOST HOME</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/style.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>

  <!-- NAVIGASI ATAS -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-success">
    <a href="<?php echo site_url('Welcome'); ?>"><img src="<?php echo base_url(); ?>/img/home-icon-silhouette.png"  class="img-fluid" style="width:180px;height:50px;" alt="logo_img"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarColor01">
      <h1 class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo site_url('Welcome'); ?>">K7-KOST <span class="sr-only">(current)</span></a>
        </li>
      </h1>
      <form class="form-inline my-2 my-lg-0">
        <div class="btn-group mr-lg-5" role="group" aria-label="Button group with nested dropdown">
          <button type="button" class="btn btn-light">Login</button>
          <div class="btn-group" role="group">
            <button id="btnGroupDrop2" type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
            <div class="dropdown-menu" aria-labelledby="btnGroupDrop2" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 48px, 0px); top: 0px; left: 0px; will-change: transform;">
              <a class="dropdown-item" href="<?php echo site_url('C_Login_Admin'); ?>">Admin</a>
              <a class="dropdown-item" href="<?php echo site_url('C_Login_User'); ?>">Member</a>
            </div>
          </div>
        </div>
      </form>
    </div>
  </nav>
