<?php

class C_Admin extends CI_Controller{

	function __construct(){
		parent::__construct();

		if($this->session->userdata('status') != "login"){
			redirect(site_url("C_Login_Admin"));
		}
	}

	function index(){
		$this->load->view('V_Admin');
	}
}
