<?php

class C_User extends CI_Controller{

	function __construct(){
		parent::__construct();

		if($this->session->userdata('status') != "login"){
			redirect(site_url("C_Login_User"));
		}
	}

	function index(){
		$this->load->view('V_User');
	}
}
