<?php

class C_Login_Admin extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('M_Login');

	}

	function index(){
		$this->load->view('Login/Head');
		$this->load->view('V_Login_Admin');
	}

	function aksi_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => $password
			);
		$cek = $this->M_Login->cek_login_admin("tb_admin",$where)->num_rows();
		if($cek > 0){

			$data_session = array(
				'nama' => $username,
				'status' => "login"
				);

			$this->session->set_userdata($data_session);

			redirect(site_url("C_Admin"));

		}else{
			echo "Username dan password salah !";
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(site_url('Welcome'));
	}
}
